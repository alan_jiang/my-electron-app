## 常用命令
启动
> npm run start

构建
> npm run make

打包
> npm run package

## 参考地址
快速入门
> https://www.electronjs.org/zh/docs/latest/tutorial/quick-start

加载Chrome扩展
> https://www.electronjs.org/zh/docs/latest/tutorial/devtools-extension

配置管理
> https://www.electronforge.io/configuration#packager-config

配置选项
> https://electron.github.io/electron-packager/main/interfaces/electronpackager.options.html

