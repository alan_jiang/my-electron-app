module.exports = {
    packagerConfig: {
        "icon": "assets/logo/app.icns",
        "name": "勘设云办公",
        "asar": true,
        "overwrite": true,
        extraResource: ['Extensions']
    },
    electronRebuildConfig: {},
    makers: [
        // Makers are Electron Forge's way of taking your packaged application and making platform specific distributables like DMG, EXE, or Flatpak files (amongst others).
        {
            name: '@electron-forge/maker-zip'
        },
        {
            "name": "@electron-forge/maker-zip",
            "platforms": [
                "darwin"
            ]
        }
    ],
    publishers: [],
    plugins: [],
    hooks: {
        generateAssets: async (forgeConfig, platform, arch) => {
            console.log('=======', forgeConfig)
            return new Promise((resolve, reject) => {
                resolve()
            })
        }
    },
    buildIdentifier: 'my-build'
}
