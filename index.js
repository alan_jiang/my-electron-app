const {app, BrowserWindow, session} = require('electron')
const path = require('path')
const os = require('os')
const Util = require('./util/util');
const { net } = require('electron')

function createWindow() {
  // We cannot require the screen module until the app is ready.
  const { screen } = require('electron')
  // Create a window that fills the screen's available work area.
  const primaryDisplay = screen.getPrimaryDisplay()
  const { width, height } = primaryDisplay.workAreaSize
  console.log('primaryDisplay', {width, height})

  const win = new BrowserWindow({
    width: width,
    height: height,
    title: "勘设云办公",
    icon: "assets/logo/app.icns",
    minWidth: width * 0.9,
    minHeight: height * 0.9,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })
  win.loadURL('http://192.168.80.216/jpaas/')
}

// 添加扩展: https://www.electronjs.org/zh/docs/latest/tutorial/devtools-extension
// 添加Office插件扩展
const vueDevToolsPath = path.join(
  os.homedir(),
  // on macOS
  '/Library/Application Support/Google/Chrome/Default/Extensions/iaajmlceplecbljialhhkmedjlpdblhp/5.3.4_0'
)
// const vueDevToolsPath = path.join(
//   __dirname,
//   'Extensions/Vue_jsDevtools/5.3.4_0'
// )

app.whenReady().then(async () => {
  console.log("====", )
  const vueExtentionPath = `${process.resourcesPath}${path.sep}Extensions${path.sep}iaajmlceplecbljialhhkmedjlpdblhp${path.sep}5.3.4_0`
  if (Util.fsExistsSync(vueExtentionPath)) {
    await session.defaultSession.loadExtension(vueExtentionPath)
  } else {
    console.warn('不存在', vueExtentionPath)
  }

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})


function loadVersion(msgEl) {
  const request =  net.request('http://192.168.80.216/jpaas/version.json')
  request.on('response', (response) => {
    console.log(`STATUS: ${response.statusCode}`)
    console.log(`HEADERS: ${JSON.stringify(response.headers)}`)
    response.on('data', (chunk) => {
      const version = JSON.parse(`${chunk}`)
      const {versionId, gitBranch, commitId} = version
    })
    response.on('end', () => {
      console.log('No more data in response.')
    })
  })
  request.end()
}
