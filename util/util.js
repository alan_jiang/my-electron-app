const fs = require('fs')
const {session} = require('electron')

const Util = {}

/**
 * 检测文件是否存在
 * @param path
 * @returns {boolean}
 */
Util.fsExistsSync = function (path) {
  try{
    fs.accessSync(path, fs.constants.F_OK);
  }catch(e){
    return false;
  }
  return true;
}

/**
 * 根据Chrome扩展ID加载扩展
 * @param id
 */
Util.loadExtensionWithId = function (id = '') {
//  :TODO
}

module.exports = Util
